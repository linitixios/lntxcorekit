//
//  LNTXSingletonTests.m
//  LNTXCoreKit
//
//  Created by Damien Rambout on 06/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "LNTXSingleton.h"

@interface LNTXSingletonTests : XCTestCase

+ (instancetype)sharedInstance;

@end

@implementation LNTXSingletonTests

LNTXSingleton(sharedInstance);

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

- (void)testSingletonMethodExists {
    XCTAssert([self.class respondsToSelector:@selector(sharedInstance)], @"Singleton method should be defined on self.");
}

- (void)testSingletonMethodReturnsNonNilValues {
    XCTAssertNotNil([self.class sharedInstance], @"Singleton values should never be nil.");
}

- (void)testSingletonMethodAlwaysReturnsTheSameValue {
    XCTAssertEqual([self.class sharedInstance], [self.class sharedInstance],
                   @"Singleton values should always be the same.");
}

@end
