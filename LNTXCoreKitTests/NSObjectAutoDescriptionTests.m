//
//  NSObjectAutoDescriptionTests.m
//  LNTXCoreKit
//
//  Created by Damien Rambout on 06/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "NSObject+LNTXAutoDescription.h"

@interface LNTXTestAutoDescriptionObject : NSObject

@property (nonatomic) id lntx_customProperty;

@end

@implementation LNTXTestAutoDescriptionObject

LNTXDefineAutoDescription();

@end

@interface NSObjectAutoDescriptionTests : XCTestCase

@property (nonatomic) LNTXTestAutoDescriptionObject *object;

@end

@implementation NSObjectAutoDescriptionTests

- (void)setUp
{
    [super setUp];

    self.object = [[LNTXTestAutoDescriptionObject alloc] init];
}

- (void)tearDown
{
    self.object = nil;
    
    [super tearDown];
}

- (void)testAutoDescriptionMethodReturnValueIsNeverNil {
    XCTAssertNotNil([self.object lntx_autoDescription],
                    @"Auto description method return value should never be nil.");
}

- (void)testDescriptionMethodsReturnsTheSameValueAsAutoDescriptionMethod {
    XCTAssertEqualObjects([self.object description], [self.object lntx_autoDescription],
                          @"Value returned by description method should be the same as the value returned by autoDescriptoin method.");
}

- (void)testAutoDescriptionContainsTheClassName {
    NSString *autoDescription = [self.object lntx_autoDescription];
    NSString *className = NSStringFromClass(self.object.class);
    XCTAssert([autoDescription rangeOfString:className].location != NSNotFound);
}

- (void)testAutoDescriptionContainsCustomProperty {
    NSString *propertyName = @"lntx_customProperty";
    NSAssert([self.object respondsToSelector:NSSelectorFromString(propertyName)],
             @"Property name does not exist");
    NSString *autoDescription = [self.object lntx_autoDescription];
    XCTAssert([autoDescription rangeOfString:propertyName].location != NSNotFound,
              @"Auto description should contain the property %@.", propertyName);
    XCTAssert([autoDescription rangeOfString:[self.object description]].location != NSNotFound,
              @"Auto description should contain the property value.");
}

@end
