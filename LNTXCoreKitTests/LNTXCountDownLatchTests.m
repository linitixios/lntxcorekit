//
//  LNTXCountDownLatchTests.m
//  LNTXCoreKit
//
//  Created by Damien Rambout on 06/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "LNTXCountDownLatch.h"
#import "LNTXBlockSelf.h"

static NSUInteger const kInitialCount = 5;

@interface LNTXCountDownLatchTests : XCTestCase

@property (nonatomic) LNTXCountDownLatch *latch;

@end

@implementation LNTXCountDownLatchTests

- (void)setUp
{
    [super setUp];
    
    self.latch = [[LNTXCountDownLatch alloc] initWithCount:kInitialCount];
}

- (void)tearDown
{
    self.latch = nil;
    
    [super tearDown];
}

- (void)countdownToZero {
    while (self.latch.count > 0) {
        [self.latch countdown];
    }
}

#pragma mark - Count value

- (void)testInitialCountIsValid {
    XCTAssert(self.latch.initialCount == kInitialCount,
              @"Initial cout value should be equal to the value passed to the initializer.");
}

- (void)testStartCountValueIsEqualToInitialCount {
    XCTAssert(self.latch.count == self.latch.initialCount,
              @"Start count value should be equal to the initial count.");
}

#pragma mark - Countdown

- (void)testCountDownDecrementsTheCountValue {
    NSUInteger count1 = self.latch.count;
    NSAssert(count1 > 0, @"Make sure the currnet count is > 0 to test this.");
    [self.latch countdown];
    
    XCTAssert(self.latch.count == count1 - 1,
              @"Countdown should decrement the latch's count value.");
}

- (void)testCountDownNeverDecrementsBelowZero {
    NSUInteger startCount = self.latch.count;
    for (int i = 0; i < startCount + 1; i++) {
        [self.latch countdown];
    }
    
    XCTAssert(self.latch.count == 0,
              @"Countdown should never decrement below zero.");
}

#pragma mark - Convenience constructor

- (void)testConvenienceConstructorReturnsANonNilValue {
    XCTAssertNotNil([LNTXCountDownLatch latchWithCount:kInitialCount],
                    @"Convenience constructor should never return a nil value.");
}

- (void)testConvenienceConstructorReturnsAValidLatch {
    LNTXCountDownLatch *latch = [LNTXCountDownLatch latchWithCount:kInitialCount];
    
    XCTAssert(latch.initialCount == kInitialCount,
              @"Convenience constructor latch's initial count should be the value passed in the constructor.");
    XCTAssert(latch.count == latch.initialCount,
              @"Convenience constructor latch's count should be equal to the initial count right after initialization.");
}

#pragma mark - Await

- (void)testAwaitWaitsUntilCountIsZero {
    LNTXPrepareWeakSelf();
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        [LNTXWeakSelf countdownToZero];
    });
    
    [self.latch await];
    XCTAssert(self.latch.count == 0, @"Latch count should be equal to zero when unlocking.");
}

- (void)testWaitUntilTimeoutWaitsUntilCountIsZero {
    LNTXPrepareWeakSelf();
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        [LNTXWeakSelf countdownToZero];
    });
    
    [self.latch awaitUntilTimeout:NSUIntegerMax];
    XCTAssert(self.latch.count == 0, @"Latch count should be equal to zero when unlocking.");
}

- (void)testAwaitHasNoEffectWhenCountIsZero {
    [self countdownToZero];
    
    long value = [self.latch awaitUntilTimeout:0.1];
    XCTAssert(value == 0, @"Await should not have any effect when count is zero.");
}

@end
