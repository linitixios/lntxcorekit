//
//  LNTXSingleton.h
//  LNTXCoreKit
//
//  Created by Damien Rambout on 06/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#ifndef LNTXCoreKit_LNTXSingleton_h
#define LNTXCoreKit_LNTXSingleton_h

#define LNTXSingleton(methodName) \
+ (id)methodName { \
    static id sharedInstance = nil; \
    static dispatch_once_t onceToken; \
    dispatch_once(&onceToken, ^{ \
        sharedInstance = [[self alloc] init]; \
    }); \
    return sharedInstance; \
}

#endif
