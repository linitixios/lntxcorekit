//
//  LNTXDispatchExtensions.h
//  LNTXCoreKit
//
//  Created by Damien Rambout on 06/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <dispatch/dispatch.h>

#ifndef __LNTX_DISPATCH_EXTENSIONS__
#define __LNTX_DISPATCH_EXTENSIONS__

#pragma mark - Main queue

#ifdef __BLOCKS__
__OSX_AVAILABLE_STARTING(__MAC_10_6,__IPHONE_4_0)
DISPATCH_EXPORT DISPATCH_NONNULL_ALL DISPATCH_NOTHROW
/**
 *  Submits a block for synchronous execution on the main queue.
 *
 * @discussion
 * See dispatch_sync() for details.
 *
 * @param block
 * The block to be invoked on the target dispatch queue.
 * The result of passing NULL in this parameter is undefined.
 */
void
dispatch_sync_main(dispatch_block_t block);
#endif

#ifdef __BLOCKS__
__OSX_AVAILABLE_STARTING(__MAC_10_6,__IPHONE_4_0)
DISPATCH_EXPORT DISPATCH_NONNULL_ALL DISPATCH_NOTHROW
/**
 *  Submits a block for asynchronous execution on the main queue.
 *
 * @discussion
 * See dispatch_async() for details.
 *
 * @param queue
 * The target dispatch queue to which the block is submitted.
 * The system will hold a reference on the target queue until the block
 * has finished.
 * The result of passing NULL in this parameter is undefined.
 *
 * @param block
 * The block to submit to the target dispatch queue. This function performs
 * Block_copy() and Block_release() on behalf of callers.
 * The result of passing NULL in this parameter is undefined.
 */
void
dispatch_async_main(dispatch_block_t block);
#endif

__OSX_AVAILABLE_STARTING(__MAC_10_6,__IPHONE_4_0)
DISPATCH_EXPORT DISPATCH_NONNULL2 DISPATCH_NOTHROW
/**
 *  Submits a function for synchronous execution on the main queue.
 *
 * @discussion
 * See dispatch_sync() for details.
 *
 * @param context
 * The application-defined context parameter to pass to the function.
 *
 * @param work
 * The application-defined function to invoke on the target queue. The first
 * parameter passed to this function is the context provided to
 * dispatch_async_f().
 * The result of passing NULL in this parameter is undefined.
 */
void
dispatch_sync_main_f(void *context, dispatch_function_t work);

__OSX_AVAILABLE_STARTING(__MAC_10_6,__IPHONE_4_0)
DISPATCH_EXPORT DISPATCH_NONNULL2 DISPATCH_NOTHROW
/**
 *  Submits a function for asynchronous execution on the main queue.
 *
 * @discussion
 * See dispatch_async() for details.
 *
 * @param context
 * The application-defined context parameter to pass to the function.
 *
 * @param work
 * The application-defined function to invoke on the target queue. The first
 * parameter passed to this function is the context provided to
 * dispatch_async_f().
 * The result of passing NULL in this parameter is undefined.
 */
void
dispatch_async_main_f(void *context, dispatch_function_t work);

#pragma mark - Background queue

#ifdef __BLOCKS__
__OSX_AVAILABLE_STARTING(__MAC_10_6,__IPHONE_4_3)
DISPATCH_EXPORT DISPATCH_NONNULL_ALL DISPATCH_NOTHROW
/**
 *  Submits a block for synchronous execution on the background queue.
 *
 * @discussion
 * See dispatch_sync() for details.
 *
 * @param block
 * The block to be invoked on the target dispatch queue.
 * The result of passing NULL in this parameter is undefined.
 */
void
dispatch_sync_background(dispatch_block_t block);
#endif

#ifdef __BLOCKS__
__OSX_AVAILABLE_STARTING(__MAC_10_6,__IPHONE_4_3)
DISPATCH_EXPORT DISPATCH_NONNULL_ALL DISPATCH_NOTHROW
/**
 *  Submits a block for asynchronous execution on the background queue.
 *
 * @discussion
 * See dispatch_async() for details.
 *
 * @param queue
 * The target dispatch queue to which the block is submitted.
 * The system will hold a reference on the target queue until the block
 * has finished.
 * The result of passing NULL in this parameter is undefined.
 *
 * @param block
 * The block to submit to the target dispatch queue. This function performs
 * Block_copy() and Block_release() on behalf of callers.
 * The result of passing NULL in this parameter is undefined.
 */
void
dispatch_async_background(dispatch_block_t block);
#endif

__OSX_AVAILABLE_STARTING(__MAC_10_6,__IPHONE_4_3)
DISPATCH_EXPORT DISPATCH_NONNULL2 DISPATCH_NOTHROW
/**
 *  Submits a function for synchronous execution on the background queue.
 *
 * @discussion
 * See dispatch_sync() for details.
 *
 * @param context
 * The application-defined context parameter to pass to the function.
 *
 * @param work
 * The application-defined function to invoke on the target queue. The first
 * parameter passed to this function is the context provided to
 * dispatch_async_f().
 * The result of passing NULL in this parameter is undefined.
 */
void
dispatch_sync_background_f(void *context, dispatch_function_t work);

__OSX_AVAILABLE_STARTING(__MAC_10_6,__IPHONE_4_3)
DISPATCH_EXPORT DISPATCH_NONNULL2 DISPATCH_NOTHROW
/**
 *  Submits a function for synchronous execution on the background queue.
 *
 * @discussion
 * See dispatch_sync() for details.
 *
 * @param context
 * The application-defined context parameter to pass to the function.
 *
 * @param work
 * The application-defined function to invoke on the target queue. The first
 * parameter passed to this function is the context provided to
 * dispatch_async_f().
 * The result of passing NULL in this parameter is undefined.
 */
void
dispatch_async_background_f(void *context, dispatch_function_t work);

#endif
