//
//  NSDate+LNTXDateComponents.m
//  LNTXCoreKit
//
//  Created by Damien Rambout on 25/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import "NSDate+LNTXDateComponents.h"

@implementation NSDate (LNTXDateComponents)

+ (instancetype)today {
    NSDateComponents *todayComponents = [[NSDate date] components:(NSCalendarUnitDay |
                                                                   NSCalendarUnitMonth |
                                                                   NSCalendarUnitYear)];
    
    return [[NSCalendar currentCalendar] dateFromComponents:todayComponents];
}

+ (instancetype)yesterday {
    return [[NSDate today] dateByAddingDays:-1];
}

+ (instancetype)tomorrow {
    return [[NSDate today] dateByAddingDays:1];
}

- (BOOL)isToday {
    NSDateComponents *todayComponents = [[NSDate today] components:(NSCalendarUnitDay |
                                                                    NSCalendarUnitMonth |
                                                                    NSCalendarUnitYear)];
    
    NSDateComponents *selfComponents = [self components:(NSCalendarUnitDay |
                                                         NSCalendarUnitMonth |
                                                         NSCalendarUnitYear)];
    
    return ((todayComponents.day == selfComponents.day) &&
            (todayComponents.month == selfComponents.month) &&
            (todayComponents.year == selfComponents.year));
}

- (NSDateComponents *)components:(NSUInteger)unitFlags {
    return [[NSCalendar currentCalendar] components:unitFlags fromDate:self];
}

- (instancetype)dateByAddingComponents:(NSDateComponents *)components {
    return [self dateByAddingComponents:components options:0];
}


- (instancetype)dateByAddingComponents:(NSDateComponents *)components options:(NSUInteger)options {
    return [[NSCalendar currentCalendar] dateByAddingComponents:components
                                                         toDate:self
                                                        options:options];
}

- (instancetype)dateByAddingSeconds:(NSInteger)numberOfSeconds {
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.second = numberOfSeconds;
    return [self dateByAddingComponents:components];
}

- (instancetype)dateByAddingMinutes:(NSInteger)numberOfMinutes {
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.minute = numberOfMinutes;
    return [self dateByAddingComponents:components];
}

- (instancetype)dateByAddingHours:(NSInteger)numberOfHours {
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.hour = numberOfHours;
    return [self dateByAddingComponents:components];
}

- (instancetype)dateByAddingDays:(NSInteger)numberOfDays {
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.day = numberOfDays;
    return [self dateByAddingComponents:components];
}

- (instancetype)dateByAddingWeeks:(NSInteger)numberOfWeeks {
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.weekOfYear = numberOfWeeks;
    return [self dateByAddingComponents:components];
}

- (instancetype)dateByAddingMonths:(NSInteger)numberOfMonths {
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.month = numberOfMonths;
    return [self dateByAddingComponents:components];
}

- (instancetype)dateByAddingYears:(NSInteger)numberOfYears {
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.year = numberOfYears;
    return [self dateByAddingComponents:components];
}

@end
