//
//  LNTXLog.h
//  LNTXCoreKit
//
//  Created by Damien Rambout on 06/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#ifndef LNTXCoreKit_LNTXLog_h
#define LNTXCoreKit_LNTXLog_h

#if DEBUG
#define LNTXLog(...) NSLog(@"%@ (%@: %d)", [NSString stringWithFormat:__VA_ARGS__], [[NSString stringWithCString:__FILE__ encoding:NSUTF8StringEncoding] lastPathComponent], __LINE__)
#else
#define LNTXLog(...)
#endif

#endif
