//
//  UIDevice+LNTXHardware.h
//  LNTXCoreKit
//
//  Created by Damien Rambout on 12/03/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIDevice (LNTXHardware)

/**
 @return Yes if the device has 64 bit hardware, NO otherwise.
 */
- (BOOL)is64bitHardware;

@end
