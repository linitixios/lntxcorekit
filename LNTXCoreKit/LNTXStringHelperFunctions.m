//
//  LNTXStringHelperFunctions.m
//  LNTXCoreKit
//
//  Created by Damien Rambout on 06/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import "LNTXStringHelperFunctions.h"

inline NSString *LNTXConcatStrings(NSString *s, ...) {
    NSMutableString *result = [NSMutableString string];
    
    va_list args;
    va_start(args, s);
    for (NSString *arg = s; arg != nil; arg = va_arg(args, NSString *))
    {
        [result appendString:arg];
    }
    va_end(args);
    
    return [result copy];
}

inline NSString *LNTXConcatStringsDiv(NSString *divider, NSString *s, ...) {
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:5];
    
    va_list args;
    va_start(args, s);
    for (NSString *arg = s; arg != nil; arg = va_arg(args, NSString *))
    {
        [array addObject:arg];
    }
    va_end(args);
    
    return [array componentsJoinedByString:divider];
}

inline NSString *LNTXctos(char c) {
    return [NSString stringWithFormat:@"%c", c];
}

inline NSString *LNTXuctos(unsigned char uc) {
    return [NSString stringWithFormat:@"%uc", uc];
}

inline NSString *LNTXitos(int i) {
    return [NSString stringWithFormat:@"%i", i];
}

inline NSString *LNTXuitos(unsigned int ui) {
    return [NSString stringWithFormat:@"%u", ui];
}

inline NSString *LNTXltos(long l) {
    return [NSString stringWithFormat:@"%ld", l];
}

inline NSString *LNTXlutos(unsigned long lu) {
    return [NSString stringWithFormat:@"%lu", lu];
}

inline NSString *LNTXlltos(long long ll) {
    return [NSString stringWithFormat:@"%lld", ll];
}

inline NSString *LNTXllutos(unsigned long long llu) {
    return [NSString stringWithFormat:@"%llu", llu];
}

inline NSString *LNTXftos(double f) {
    return [NSString stringWithFormat:@"%f", f];
}

inline NSString *LNTXLftos(long double Lf) {
    return [NSString stringWithFormat:@"%Lf", Lf];
}

inline NSString *LNTXxtos(unsigned int x) {
    return [NSString stringWithFormat:@"%x", x];
}

inline NSString *LNTXotos(unsigned int o) {
    return [NSString stringWithFormat:@"%o", o];
}

inline NSString *LNTXptos(void *p) {
    return [NSString stringWithFormat:@"%p", p];
}

inline NSString *LNTXnsitos(NSInteger nsi) {
    return [@(nsi) stringValue];
}

inline NSString *LNTXnsuitos(NSUInteger nsui) {
    return [@(nsui) stringValue];
}

inline NSString *LNTXcgftos(CGFloat cgf) {
    return [@(cgf) stringValue];
}
