//
//  UIApplication+LNTXArchitecture.h
//  LNTXCoreKit
//
//  Created by Damien Rambout on 12/03/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIApplication (LNTXArchitecture)

/**
 @return YES if the application is running the 32 bit version of the binary, NO otherwise.
 */
- (BOOL)isUsing32bitBinary;

/**
 @return YES if the application is running the 64 bit version of the binary, NO otherwise.
 */
- (BOOL)isUsing64bitBinary;

@end
