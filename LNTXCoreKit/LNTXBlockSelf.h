//
//  LNTXBlockSelf.h
//  LNTXCoreKit
//
//  Created by Damien Rambout on 06/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#ifndef LNTXCoreKit_LNTXBlockSelf_h
#define LNTXCoreKit_LNTXBlockSelf_h

#define LNTXWeakSelf __weakSelf
#define LNTXStrongBlockSelf __strongBlockSelf
#define LNTXPrepareWeakSelf() __weak typeof(self) LNTXWeakSelf = self
#define LNTXPrepareStrongBlockSelf() __strong typeof(LNTXWeakSelf) LNTXStrongBlockSelf = LNTXWeakSelf

#endif
