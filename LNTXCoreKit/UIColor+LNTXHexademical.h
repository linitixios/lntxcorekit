//
//  UIColor+LNTXHexademical.h
//  LNTXCoreKit
//
//  Created by Damien Rambout on 20/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 Category for creating UIColor objects from hexademical values.
 */
@interface UIColor (LNTXHexademical)

/**
 Returns a UIColor object from an hexademical value.
 @return A UIColor object from an hexademical value.
 */
+ (instancetype)lntx_colorWithHexValue:(unsigned int)hexValue;

/**
 Returns a UIColor object from an hexademical string value. 
 The string value can be prefixed with a '#' character. Accepted formats are '#RBG', '#ARGB', '#RRGGBB' and '#AARRGGBB'.
 @return A UIColor object from an hexademical string value.
 */
+ (instancetype)lntx_colorWithHexString:(NSString *)hexString;

@end
