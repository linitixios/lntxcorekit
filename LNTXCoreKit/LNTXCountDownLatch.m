//
//  LNTXCountDownLatch.m
//  LNTXCoreKit
//
//  Created by Damien Rambout on 06/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import "LNTXCountDownLatch.h"

#import "LNTXBlockSelf.h"
#import "LNTXDispatchExtensions.h"

@interface LNTXCountDownLatch ()

@property (nonatomic) dispatch_semaphore_t semaphore;
@property (nonatomic) NSUInteger initialCount;
@property (nonatomic) NSUInteger count;

@end

@implementation LNTXCountDownLatch

+ (instancetype)latchWithCount:(NSUInteger)count {
    return [[self alloc] initWithCount:count];
}

- (instancetype)initWithCount:(NSUInteger)count {
    if (self = [super init]) {
        self.semaphore = dispatch_semaphore_create(0);
        self.initialCount = count;
        self.count = count;
    }
    
    return self;
}

- (void)await {
    [self awaitTime:DISPATCH_TIME_FOREVER];
}

- (void)awaitAsynchronouslyWithCallback:(void(^)())callback {
    [self awaitAsynchronouslyTime:DISPATCH_TIME_FOREVER callback:^(long timeout) {
        callback();
    }];
}

- (void)awaitAsynchronouslyUntilTimeout:(NSTimeInterval)timeoutInSeconds callback:(void(^)(long timeout))callback {
    [self awaitAsynchronouslyTime:dispatch_walltime(DISPATCH_TIME_NOW,
                                                    (int64_t)(timeoutInSeconds * NSEC_PER_SEC))
                         callback:callback];
}

- (void)awaitAsynchronouslyTime:(dispatch_time_t)time callback:(void(^)(long timeout))callback {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        long timeout = [self awaitTime:time];
        dispatch_async_main(^{
            callback(timeout);
        });
    });
}

- (long)awaitUntilTimeout:(NSTimeInterval)timeoutInSeconds {
    return [self awaitTime:dispatch_walltime(DISPATCH_TIME_NOW,
                                             (int64_t)(timeoutInSeconds * NSEC_PER_SEC))];
}

- (long)awaitTime:(dispatch_time_t)timeout {
    if (self.count > 0) {
        return dispatch_semaphore_wait(self.semaphore, timeout);
    }
    
    return 0;
}

- (void)countdown {
    @synchronized (self) {
        if (self.count > 0) {
            self.count--;
            
            if (self.count == 0) {
                long theadWoken = 1;
                while (theadWoken != 0) {
                    theadWoken = dispatch_semaphore_signal(self.semaphore);
                }
            }
        }
    }
}

@end
