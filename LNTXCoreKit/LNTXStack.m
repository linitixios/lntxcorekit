//
//  LNTXStack.m
//  LNTXCoreKit
//
//  Created by Damien Rambout on 10/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import "LNTXStack.h"

#import <NSObject+LNTXAutoDescription.h>

@implementation NSMutableArray (LNTXStack)

+ (instancetype)stack {
    return [[self alloc] init];
}

- (void)pushObject:(id)object {
    [self insertObject:object atIndex:0];
}

- (id)popObject {
    id object = [self objectAtIndex:0];
    [self removeObjectAtIndex:0];
    
    return object;
}

- (id)topObject {
    return [self firstObject];
}

@end
