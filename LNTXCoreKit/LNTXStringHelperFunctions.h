//
//  LNTXStringHelperFunctions.h
//  LNTXCoreKit
//
//  Created by Damien Rambout on 06/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CGBase.h>

/** 
 Concats all string passed as parameter. No divider is used.
 */
extern NSString *LNTXConcatStrings(NSString *s, ...) NS_REQUIRES_NIL_TERMINATION;
/**
 Concats all string passed as parameter. Each string is separated with the divider provided.
 */
extern NSString *LNTXConcatStringsDiv(NSString *divider, NSString *s, ...) NS_REQUIRES_NIL_TERMINATION;

/**
 Converts a char into a NSString.
 */
extern NSString *LNTXctos(char c);
/**
 Converts an unsigned char into a NSString.
 */
extern NSString *LNTXuctos(unsigned char uc);

/**
 Converts an int into a NSString.
 */
extern NSString *LNTXitos(int i);
/**
 Converts an unsigned int into a NSString.
 */
extern NSString *LNTXuitos(unsigned int ui);

/**
 Converts a long into a NSString.
 */
extern NSString *LNTXltos(long l);
/**
 Converts an unsigned long into a NSString.
 */
extern NSString *LNTXlutos(unsigned long lu);
/**
 Converts a long long into a NSString.
 */
extern NSString *LNTXlltos(long long ll);
/**
 Converts an unsigned long long into a NSString.
 */
extern NSString *LNTXllutos(unsigned long long llu);

/**
 Converts a double into a NSString.
 */
extern NSString *LNTXftos(double f);
/**
 Converts a long double into a NSString.
 */
extern NSString *LNTXLftos(long double Lf);

/**
 Converts an unsigned int into an hexadecimal NSString.
 */
extern NSString *LNTXxtos(unsigned int x);
/**
 Converts an unsigned int into an hexadecimal NSString. The output string will start with 0x.
 */
extern NSString *LNTXatos(unsigned int a);
/**
 Converts an unsigned int into an octal NSString.
 */
extern NSString *LNTXotos(unsigned int o);
/**
 Converts an address pointer a NSString.
 */
extern NSString *LNTXptos(void *p);

/**
 Converts a NSInteger into a NSString.
 @param nsi The value to convert.
 @return A NSString representation of the NSInteger parameter.
 */
extern NSString *LNTXnsitos(NSInteger nsi);
#define NSitos(nsi) LNTXnsitos(nsi)

/**
 Converts a NSUInteger into a NSString.
 @param nsui The value to convert.
 @return A NSString representation of the NSUInteger parameter.
 */
extern NSString *LNTXnsuitos(NSUInteger nsui);
#define NSuitos(nsui) LNTXnsuitos(nsui)

/**
 Converts a CGFloat into a NSString.
 @param cgf The value to convert.
 @return A NSString representation of the CGFloat parameter.
 */
extern NSString *LNTXcgftos(CGFloat cgf);
#define CGftos(cgf) LNTXcgftos(cgf)
