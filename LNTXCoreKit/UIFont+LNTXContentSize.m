//
//  UIFont+LNTXContentSize.m
//  LNTXCoreKit
//
//  Created by Damien Rambout on 20/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import "UIFont+LNTXContentSize.h"

#if !defined(LNTX_APP_EXTENSIONS)

static NSArray *kContentSizeCategories;
static CGFloat const kMinFontSize = 12.0f;

@implementation UIFont (LNTXContentSize)

+ (void)initialize {
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(preferredContentSizeCategory)]) {
        // Ordered content size categories
        kContentSizeCategories = @[UIContentSizeCategoryExtraSmall,
                                   UIContentSizeCategorySmall,
                                   UIContentSizeCategoryMedium,
                                   // Default value (Large)
                                   UIContentSizeCategoryLarge,
                                   UIContentSizeCategoryExtraLarge,
                                   UIContentSizeCategoryExtraExtraLarge,
                                   UIContentSizeCategoryExtraExtraExtraLarge];
    }
}

+ (CGFloat)lntx_fontSizeByScalingSize:(CGFloat)fontSize {
    CGFloat scaledFontSize = fontSize;
    
    // Check whether the platform supports dynamic content size
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(preferredContentSizeCategory)]) {
        NSString *preferredContentSizeCategory = [UIApplication sharedApplication].preferredContentSizeCategory;
        if ([kContentSizeCategories indexOfObject:preferredContentSizeCategory] != NSNotFound) {
            // Get the content size category index and compute different with the default one
            NSInteger contentSizeDelta = [kContentSizeCategories indexOfObject:preferredContentSizeCategory];
            contentSizeDelta -= [kContentSizeCategories indexOfObject:UIContentSizeCategoryLarge];
            // Add the index difference to the font size
            scaledFontSize = MAX(kMinFontSize, roundf(scaledFontSize + contentSizeDelta));
        }
    }
    
    return  scaledFontSize;
}

+ (UIFont *)lntx_fontWithName:(NSString *)fontName size:(CGFloat)fontSize scaledForContentSize:(BOOL)scaledForContentSize {
    return [UIFont fontWithName:fontName size:(scaledForContentSize ? [self lntx_fontSizeByScalingSize:fontSize] : fontSize)];
}

@end

#endif
