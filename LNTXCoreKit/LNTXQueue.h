//
//  LNTXQueue.h
//  LNTXCoreKit
//
//  Created by Damien Rambout on 20/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NSMutableArray LNTXQueue;

/** 
 @brief A NSMutableArray category for simulating a mutable queue data structure.
 */
@interface NSMutableArray (LNTXQueue)

+ (instancetype)queue;

/**
 @brief Inserts the specified element at the end of this queue.
 @param object The object to insert in the queue.
 */
- (void)enqueue:(id)object;

/**
 @brief Returns and remove the head of this queue. An exception is raised if the queue is empty.
 @return The head of this queue.
 */
- (id)dequeue;

/**
 @brief Returns, but does not remove, the head of this queue, or returns `nil` if this queue is empty.
 @return The head of this queue, or returns `nil` if this queue is empty.
 */
- (id)peekObject;

@end
