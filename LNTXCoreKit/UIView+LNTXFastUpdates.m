//
//  UIView+LNTXFastUpdates.m
//  LNTXCoreKit
//
//  Created by Damien Rambout on 25/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import "UIView+LNTXFastUpdates.h"

@implementation UIView (LNTXFastUpdates)

#pragma mark - Origin

- (CGPoint)origin {
    return self.frame.origin;
}

- (void)setOrigin:(CGPoint)origin {
    CGRect viewFrame = self.frame;
    viewFrame.origin = origin;
    self.frame = viewFrame;
}

- (CGFloat)x {
    return CGRectGetMinX(self.frame);
}

- (void)setX:(CGFloat)x {
    CGRect viewFrame = self.frame;
    viewFrame.origin.x = x;
    self.frame = viewFrame;
}

- (CGFloat)y {
    return CGRectGetMinY(self.frame);
}

- (void)setY:(CGFloat)y {
    CGRect viewFrame = self.frame;
    viewFrame.origin.y = y;
    self.frame = viewFrame;
}

#pragma mark - Size

- (CGSize)size {
    return self.frame.size;
}

- (void)setSize:(CGSize)size {
    CGRect viewFrame = self.frame;
    viewFrame.size = size;
    self.frame = viewFrame;
}

- (CGFloat)width {
    return CGRectGetWidth(self.frame);
}

- (void)setWidth:(CGFloat)width {
    CGRect viewFrame = self.frame;
    viewFrame.size.width = width;
    self.frame = viewFrame;
}

- (CGFloat)height {
    return CGRectGetHeight(self.frame);
}

- (void)setHeight:(CGFloat)height {
    CGRect viewFrame = self.frame;
    viewFrame.size.height = height;
    self.frame = viewFrame;
}

#pragma mark - Center

- (CGFloat)xCenter {
    return self.center.x;
}

- (void)setXCenter:(CGFloat)xCenter {
    [self setRoundedCenter:CGPointMake(xCenter, self.center.y)];
}

- (CGFloat)yCenter {
    return self.center.y;
}

- (void)setYCenter:(CGFloat)yCenter {
    [self setRoundedCenter:CGPointMake(self.center.x, yCenter)];
}

- (void)setRoundedCenter:(CGPoint)center {
    self.center = center;
    
    // Round origin values
    CGRect frame = self.frame;
    frame.origin.x = roundf(CGRectGetMinX(self.frame));
    frame.origin.y = roundf(CGRectGetMinY(self.frame));
    self.frame = frame;
}

- (void)centerInView:(UIView *)containerView {
    CGRect containerFrame = containerView.frame;
    [self setRoundedCenter:CGPointMake(CGRectGetMidX(containerFrame),
                                       CGRectGetMidY(containerFrame))];
}

- (void)centerHorizontallyInView:(UIView *)containerView {
    self.xCenter = CGRectGetMidX(containerView.frame);
}

- (void)centerVerticallyInView:(UIView *)containerView {
    self.yCenter = CGRectGetMidY(containerView.frame);
}

@end
