//
//  LNTXDispatchExtensions.m
//  LNTXCoreKit
//
//  Created by Damien Rambout on 06/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import "LNTXDispatchExtensions.h"

#ifdef __BLOCKS__
inline void dispatch_sync_main(dispatch_block_t block)
{
    dispatch_sync(dispatch_get_main_queue(), block);
}
#endif

#ifdef __BLOCKS__
inline void dispatch_async_main(dispatch_block_t block)
{
    dispatch_async(dispatch_get_main_queue(), block);
}
#endif

inline void dispatch_sync_main_f(void *context, dispatch_function_t work)
{
    dispatch_sync_f(dispatch_get_main_queue(), context, work);
}

inline void dispatch_async_main_f(void *context, dispatch_function_t work)
{
    dispatch_async_f(dispatch_get_main_queue(), context, work);
}

#ifdef __BLOCKS__
inline void dispatch_sync_background(dispatch_block_t block)
{
    dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), block);
}
#endif

#ifdef __BLOCKS__
inline void dispatch_async_background(dispatch_block_t block)
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), block);
}
#endif

inline void dispatch_sync_background_f(void *context, dispatch_function_t work)
{
    dispatch_sync_f(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), context, work);
}

inline void dispatch_async_background_f(void *context, dispatch_function_t work)
{
    dispatch_async_f(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), context, work);
}
