//
//  LNTXActivityIndicatorManager.h
//  LNTXCoreKit
//
//  Created by Damien Rambout on 20/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <Foundation/Foundation.h>

#if !defined(LNTX_APP_EXTENSIONS)

/**
 A utility class for manager the activity indicator status.
 */
@interface LNTXActivityIndicatorManager : NSObject

/**
 @return A unique instance of the activity indicator manager.
 */
+ (instancetype)sharedManager;

/**
 @return The current activity indicator count.
 */
@property (nonatomic, readonly) NSInteger activityCount;

/**
 Increases the activity indicator count value of 1.
 */
- (void)increaseActivityCount;
/**
 Decreases the activity indicator count value of 1.
 */
- (void)decreaseActivityCount;

#define LNTXGetActivityIndicatorCount() [[LNTXActivityIndicatorManager sharedManager] activityCount]
#define LNTXIncreaseActivityIndicatorCount() [[LNTXActivityIndicatorManager sharedManager] increaseActivityCount]
#define LNTXDecreaseActivityIndicatorCount() [[LNTXActivityIndicatorManager sharedManager] decreaseActivityCount]

@end

#endif
