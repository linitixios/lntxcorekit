//
//  LNTXActivityIndicatorManager.m
//  LNTXCoreKit
//
//  Created by Damien Rambout on 20/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import "LNTXActivityIndicatorManager.h"

#import "LNTXSingleton.h"

#import <UIKit/UIKit.h>

#if !defined(LNTX_APP_EXTENSIONS)

@interface LNTXActivityIndicatorManager ()

@property (nonatomic) NSInteger activityCount;

@end

@implementation LNTXActivityIndicatorManager

LNTXSingleton(sharedManager);

- (void)increaseActivityCount {
    @synchronized (self) {
        self.activityCount++;
        [UIApplication sharedApplication].networkActivityIndicatorVisible = (self.activityCount > 0);
    }
}

- (void)decreaseActivityCount {
    @synchronized (self) {
        self.activityCount--;
        [UIApplication sharedApplication].networkActivityIndicatorVisible = (self.activityCount > 0);
    }
}

@end

#endif
