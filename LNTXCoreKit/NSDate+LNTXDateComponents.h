//
//  NSDate+LNTXDateComponents.h
//  LNTXCoreKit
//
//  Created by Damien Rambout on 25/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @brief Extension adding method for easy date components computations.
 */
@interface NSDate (LNTXDateComponents)

/** 
 @brief Returns today at midnight.
 @return Today at midnight.
 */
+ (instancetype)today;

/** 
 @brief Returns yesterday at midnight
 @return Yesterday at midnight.
 */
+ (instancetype)yesterday;

/**
 @brief Returns tomorrow at midnight
 @return Tomorrow at midnight.
 */
+ (instancetype)tomorrow;

/**
 @brief Returns `YES` if the receiver is today.
 @return `YES` if the receiver is today.
 */
- (BOOL)isToday;

/**
 @brief Returns the requested date components, based on the current calendar.
 @param unitFlags The components into which to decompose date—a bitwise OR of NSCalendarUnit constants.
 @return The requested date components, based on the current calendar.
 */
- (NSDateComponents *)components:(NSUInteger)unitFlags;

/**
 @brief Returns a new NSDate object representing the absolute time calculated by adding given components to the receiver date.
 @param components The components from which to calculate the returned date.
 @return A new NSDate object representing the absolute time calculated by adding given components to the receiver date. 
 Returns nil if date falls outside the defined range of the current calendar or if the computation cannot be performed.
 */
- (instancetype)dateByAddingComponents:(NSDateComponents *)components;

/**
 @brief Returns a new NSDate object representing the absolute time calculated by adding given components to the receiver date.
 @param components The components from which to calculate the returned date.
 @param options Options for the calculation. See “NSDateComponents wrapping behavior” for possible values. Pass 0 to specify no options.
 If you specify no options (you pass 0), overflow in a unit carries into the higher units (as in typical addition).
 @return A new NSDate object representing the absolute time calculated by adding given components to the receiver date.
 Returns nil if date falls outside the defined range of the current calendar or if the computation cannot be performed.
 */
- (instancetype)dateByAddingComponents:(NSDateComponents *)components options:(NSUInteger)options;

/**
 Returns a new NSDate object based on the receiver where the provided seconds have been added.
 @param seconds The amount of seconds to add to the receiver to create a new date. The value can be negative.
 @return A new NSDate object based on the receiver where the provided seconds have been added.
 */
- (instancetype)dateByAddingSeconds:(NSInteger)seconds;

/**
 Returns a new NSDate object based on the receiver where the provided minutes have been added.
 @param seconds The amount of minutes to add to the receiver to create a new date. The value can be negative.
 @return A new NSDate object based on the receiver where the provided minutes have been added.
 */
- (instancetype)dateByAddingMinutes:(NSInteger)minutes;

/**
 Returns a new NSDate object based on the receiver where the provided hours have been added.
 @param seconds The amount of hours to add to the receiver to create a new date. The value can be negative.
 @return A new NSDate object based on the receiver where the provided hours have been added.
 */
- (instancetype)dateByAddingHours:(NSInteger)hours;

/**
 Returns a new NSDate object based on the receiver where the provided days have been added.
 @param seconds The amount of days to add to the receiver to create a new date. The value can be negative.
 @return A new NSDate object based on the receiver where the provided days have been added.
 */
- (instancetype)dateByAddingDays:(NSInteger)days;

/**
 Returns a new NSDate object based on the receiver where the provided weeks have been added.
 @param seconds The amount of weeks to add to the receiver to create a new date. The value can be negative.
 @return A new NSDate object based on the receiver where the provided weeks have been added.
 */
- (instancetype)dateByAddingWeeks:(NSInteger)weeks;

/**
 Returns a new NSDate object based on the receiver where the provided months have been added.
 @param seconds The amount of months to add to the receiver to create a new date. The value can be negative.
 @return A new NSDate object based on the receiver where the provided months have been added.
 */
- (instancetype)dateByAddingMonths:(NSInteger)months;

/**
 Returns a new NSDate object based on the receiver where the provided years have been added.
 @param seconds The amount of years to add to the receiver to create a new date. The value can be negative.
 @return A new NSDate object based on the receiver where the provided years have been added.
 */
- (instancetype)dateByAddingYears:(NSInteger)years;

@end
