//
//  LNTXQueue.m
//  LNTXCoreKit
//
//  Created by Damien Rambout on 20/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import "LNTXQueue.h"

@implementation NSMutableArray (LNTXQueue)

+ (instancetype)queue {
    return [[LNTXQueue alloc] init];
}

- (void)enqueue:(id)object {
    [self addObject:object];
}

- (id)dequeue {
    id object = [self objectAtIndex:0];
    [self removeObjectAtIndex:0];
    return object;
}

- (id)peekObject {
    return [self firstObject];
}

@end
