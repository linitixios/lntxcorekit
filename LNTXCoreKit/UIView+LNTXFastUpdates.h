//
//  UIView+LNTXFastUpdates.h
//  LNTXCoreKit
//
//  Created by Damien Rambout on 25/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 @brief Category which adds properties and methods for quickly accessing and updating a view's position properties.
 */
@interface UIView (LNTXFastUpdates)

#pragma mark - Origin
/** The view's frame origin. */
@property (nonatomic) CGPoint origin;
/** The view's frame x origin. */
@property (nonatomic) CGFloat x;
/** The view's frame y origin. */
@property (nonatomic) CGFloat y;

#pragma mark - Size
/** The view's frame size. */
@property (nonatomic) CGSize size;
/** The view's frame width. */
@property (nonatomic) CGFloat width;
/** The view's frame height. */
@property (nonatomic) CGFloat height;

#pragma mark - Center
/** The view's x center. */
@property (nonatomic) CGFloat xCenter;
/** The view's y center. */
@property (nonatomic) CGFloat yCenter;

/**
 @brief Sets the center of the receiver view and rounds its frame's origin values.
 @param The new center of the view.
 */
- (void)setRoundedCenter:(CGPoint)center;

/**
 @brief Center the receiver's view in a specified container view. Receiver's frame origin values are rounded as well.
 @param The container view in which the receiver must be centered.
 */
- (void)centerInView:(UIView *)containerView;

/**
 @brief Center the receiver's view horizontally in a specified container view. Receiver's frame origin values are 
 rounded as well.
 @param The container view in which the receiver must be centered horizontally.
 */
- (void)centerHorizontallyInView:(UIView *)containerView;

/**
 @brief Center the receiver's view vertically in a specified container view. Receiver's frame origin values are rounded 
 as well.
 @param The container view in which the receiver must be centered vertically.
 */
- (void)centerVerticallyInView:(UIView *)containerView;

@end
