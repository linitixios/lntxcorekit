//
//  UIFont+LNTXContentSize.h
//  LNTXCoreKit
//
//  Created by Damien Rambout on 20/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <UIKit/UIKit.h>

#if !defined(LNTX_APP_EXTENSIONS)

/**
 @brief A category for creating UIFont objects scaled for the preferred user content size.
 */
@interface UIFont (LNTXContentSize)

/**
 @brief Creates and returns a font object for the specified font name and size. If the parameter scaled is set to YES, the font
 will be scaled based on the user content size preferences.
 @param name The fully specified name of the font. This name incorporates both the font family name and the specific 
 style information for the font.
 @param size The size (in points) to which the font is scaled. This value must be greater than 0.0.
 @param scaledForContentSize Whether the font should be scaled based on user content size preferences.
 @return A scaled font object of the specified name and size.
 */
+ (UIFont *)lntx_fontWithName:(NSString *)fontName size:(CGFloat)fontSize scaledForContentSize:(BOOL)scaledForContentSize;

@end

#endif
