//
//  UIApplication+LNTXArchitecture.m
//  LNTXCoreKit
//
//  Created by Damien Rambout on 12/03/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import "UIApplication+LNTXArchitecture.h"

@implementation UIApplication (LNTXArchitecture)

- (BOOL)isUsing32bitBinary {
    return (sizeof(void *) == 4);
}

- (BOOL)isUsing64bitBinary {
    return (sizeof(void *) == 8);
}

@end
