//
//  LNTXCountDownLatch.h
//  LNTXCoreKit
//
//  Created by Damien Rambout on 06/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @brief A synchronization aid that allows one or more threads to wait until a set of operations being performed in other
 threads completes.
 <br/>
 A LNTXCountDownLatch is initialized with a given count. The await methods block until the current count reaches zero 
 due to invocations of the countDown method, after which all waiting threads are released and any subsequent
 invocations of await return immediately. This is a one-shot phenomenon -- the count cannot be reset.
 <br/>
 A LNTXCountDownLatch is a versatile synchronization tool and can be used for a number of purposes. A CountDownLatch
 initialized with a count of one serves as a simple on/off latch, or gate: all threads invoking await wait at the gate 
 until it is opened by a thread invoking countDown. A CountDownLatch initialized to N can be used to make one thread
 wait until N threads have completed some action, or some action has been completed N times.
 <br/>
 A useful property of a LNTXCountDownLatch is that it doesn't require that threads calling countDown wait for the count to
 reach zero before proceeding, it simply prevents any thread from proceeding past an await until all threads could pass.
 */
@interface LNTXCountDownLatch : NSObject

/**
 @brief Constructs a LNTXCountDownLatch initialized with the given count.
 @param count The latch count.
 @return A LNTXCountDownLatch initialized with the given count.
 */
+ (instancetype)latchWithCount:(NSUInteger)count;

/** The initial count. */
@property (nonatomic, readonly) NSUInteger initialCount;
/** The current count. */
@property (nonatomic, readonly) NSUInteger count;

/**
 @brief Constructs a LNTXCountDownLatch initialized with the given count.
 @param count The latch count.
 @return A LNTXCountDownLatch initialized with the given count.
 */
- (instancetype)initWithCount:(NSUInteger)count;

/** 
 @brief Causes the current thread to wait until the latch has counted down to zero, unless the thread is interrupted.
 */
- (void)await;

/**
 @brief Causes the current thread to wait until the latch has counted down to zero, unless the thread is interrupted, or the
 specified waiting time elapses.
 @param timeoutInSeconds A timeout in seconds
 @return Zero on success, or non-zero if the timeout occurred.
 */
- (long)awaitUntilTimeout:(NSTimeInterval)timeoutInSeconds;

/**
 @brief Call the callback when the receiver unlocks. The block is always called back on the main thread.
 @param callback A block that will be called back when the receiver unlocks.
 */
- (void)awaitAsynchronouslyWithCallback:(void(^)())callback;

/**
 @brief Call the callback when the receiver unlocks or a timeout occurs. The value of the callback timeout parameter is
 zero on success, or non-zero if the timeout occurred.  The block is always called back on the main thread.
 @param callback A block that will be called back when the receiver unlocks or a timeout occurs.
 */
- (void)awaitAsynchronouslyUntilTimeout:(NSTimeInterval)timeoutInSeconds callback:(void(^)(long timeout))callback;

/**
 @brief Decrements the count of the latch, releasing all waiting threads if the count reaches zero.
 */
- (void)countdown;

@end
