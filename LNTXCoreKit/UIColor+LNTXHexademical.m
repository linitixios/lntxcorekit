//
//  UIColor+LNTXHexademical.m
//  LNTXCoreKit
//
//  Created by Damien Rambout on 20/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import "UIColor+LNTXHexademical.h"

#import "LNTXCoreKit.h"

@implementation UIColor (LNTXHexademical)

+ (instancetype)lntx_colorWithHexValue:(unsigned int)hexValue {
    int a = 0XFF;
    int r, g ,b;
    
    int aFound = (hexValue >> 24) & 0xFF;
    if (aFound != 0) {
        a = aFound;
    }
    
    r = (hexValue >> 16) & 0xFF;
    g = (hexValue >> 8) & 0xFF;
    b = (hexValue) & 0xFF;
    
    return [UIColor colorWithRed:(r / 255.0f) green:(g / 255.0f) blue:(b / 255.0f) alpha:(a / 255.0f)];
}

+ (instancetype)lntx_colorWithHexString:(NSString *)hexString {
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString:@"#" withString:@""] uppercaseString];
    CGFloat alpha, red, blue, green;
    
    switch ([colorString length]) {
        case 3: {
            // #RGB
            alpha = 1.0f;
            red   = [self colorComponentFrom:colorString start:0 length:1];
            green = [self colorComponentFrom:colorString start:1 length:1];
            blue  = [self colorComponentFrom:colorString start:2 length:1];
            break;
        }
            
        case 4: {
            // #ARGB
            alpha = [self colorComponentFrom:colorString start:0 length:1];
            red   = [self colorComponentFrom:colorString start:1 length:1];
            green = [self colorComponentFrom:colorString start:2 length:1];
            blue  = [self colorComponentFrom:colorString start:3 length:1];
            break;
        }
            
        case 6: {
            // #RRGGBB
            alpha = 1.0f;
            red   = [self colorComponentFrom:colorString start: 0 length:2];
            green = [self colorComponentFrom:colorString start: 2 length:2];
            blue  = [self colorComponentFrom:colorString start: 4 length:2];
            break;
        }
            
        case 8: {
            // #AARRGGBB
            alpha = [self colorComponentFrom:colorString start:0 length:2];
            red   = [self colorComponentFrom:colorString start:2 length:2];
            green = [self colorComponentFrom:colorString start:4 length:2];
            blue  = [self colorComponentFrom:colorString start:6 length:2];
            break;
        }
            
        default:
            [NSException raise:@"LNTXInvalidColorValue"
                        format:@"Color value %@ is invalid.  It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString];
            break;
    }
    
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}


+ (CGFloat)colorComponentFrom:(NSString *)string start:(NSUInteger)start length:(NSUInteger)length {
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned int hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt:&hexComponent];
    
    return hexComponent / 255.0;
}


@end
