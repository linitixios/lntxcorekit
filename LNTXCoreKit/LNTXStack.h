//
//  LNTXStack.h
//  LNTXCoreKit
//
//  Created by Damien Rambout on 10/02/14.
//  Copyright (c) 2014 Linitix. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NSMutableArray LNTXStack;

/**
 @brief A NSMutableArray category for simulating a mutable stack data structure.
 */
@interface NSMutableArray (LNTXStack)

/**
 @brief Returns an empty mutable stack.
 @return An empty mutable stack.
 */
+ (instancetype)stack;

/**
 @brief Add the object on the top of the stack.
 @param object The object to add to the stack.
 */
- (void)pushObject:(id)object;
/**
 @brief Removes and returns the object on top of the stack. An exception is raised if the stack is empty.
 @return The object removed from the top of the stack.
 */
- (id)popObject;

/**
 @brief Returns the object on top of the stack. If the stack is empty, `nil` is returned.
 @return The object on top of the stack. If the stack is empty, `nil` is returned.
 */
- (id)topObject;

@end
