# LNTXCoreKit

`LNTXCoreKit` provides several basic classes and functions that are reusable in any project.

## Installation

Make sure you have added the private **LINITIX** CocoaPods-Specs repository to your CocoaPods repository. To add it, run the following command in your terminal:

```
$ pod repo add LINITIX https://bitbucket.org/linitixios/cocoapods-specs.git
```

LNTXCoreKit is available through **LINITIX**'s private CocoaPods repository. To install
it simply add the following line to your Podfile:

```
pod "LNTXCoreKit"
```

## Cocoapods & Extensions

App extensions might have to define the `LNTX_APP_EXTENSIONS` preprocessor macro in order to avoid compiling unavailable classes or methods from `UIKit`.

First, separate pods from your main target and your extensions to only include required pods in your extensions.

Check the name of the `LNTXCoreKit` pod target. Our example assumes it is named `Pods-LNTXCoreKit`.

Add the following block to your `Podfile`:

```
post_install do |installer|
    # Setup preprocessor macros for extensions
    installer.project.targets.each do |target|
        if target.name == "Pods-LNTXCoreKit"
            target.build_configurations.each do |config|
                config.build_settings['GCC_PREPROCESSOR_DEFINITIONS'] = ['$(inherited)', 'LNTX_APP_EXTENSIONS=1']
            end
        end
    end
end
```

## Author

Damien Rambout, damien.rambout@linitix.com

*LINITIX All Rights Reserved.*


