# LNTXCoreKit CHANGELOG

## 0.5.0

- **Forced** iOS 8.
- **Fixed** deprecations.
- **Updated** project from library to framework.
- **Added** `LNTX_APP_EXTENSIONS` guard macro for app extensions.

## 0.4.2

- **Fixed** `LNTXCountDownLatch` for asynchronous callbacks.

## 0.4.1

- **Updated** The project architecture to include 64-bit.
- **Fixed** `LNTXActivityIndicatorManager` macros.

## 0.4.0

- **Added** `NSDate+LNTXDateComponents`: A `NSDate` category for easily adding date components to a specific date.
- **Added** `UIView+LNTXFastUpdates`: A `UIView` category for quickly updating position and size properties of a specific view.
- **Added** Asynchronous methods on `LNTXCountDownLatch` for asynchronously waiting (with callback) for the latch to unlock.

## 0.3.0

- **Added** `UIColor+LNTXHexadecimal`: A `UIColor` category for creating colors from hexadecimal values.
- **Added** `UIFont+LNTXContentSize`: A `UIFont` category for creating font dynamically scaled based on the preferred content size.
- **Added** `LNTXActivityIndicatorManager`: A utility class for easily managing the activity indicator status.
- **Added** `LNTXStack`: A `NSMutableArray` category for creating stack data structures.
- **Added** `LNTXQueue`: A `NSMutableArray` category for creating queue data structures.

## 0.2.0

- **Fixed** `NSObject+LNTXAutoDescription` for 64-bit architecture.
- **Added** `LNTXStack`: A class to create a stack data structure.

## 0.1.0

Initial release.

- **Added** `LNTXBlockSelf`: Macros for creating weak and strong versions of self for blocks.
- **Added** `LNTXSingleton`: Macro for creating a singleton.
- **Added** `LNTXDispatchExtensions`: Additional functions for dispatching on the main thread.
- **Added** `NSObject+LNTXAutoDescription`: Category adding an autodescription method for any NSObject instance.
- **Added** `LNTXCountDownLatch`: Tool for blocking threads until a specific count reaches zero.
- **Added** `LNTXLog`: Macro replacing NSLog that adds interesting log information and that is disabled in Release mode.
- **Added** `LNTXStringHelperFunctions`: Several functions for concatenating strings or converting values into strings.